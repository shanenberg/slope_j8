/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

package sloc;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import shapuc.IO.IO;
import shapuc.util.Reflection;
import sloc.parsing.CVSHeaderParserTokens;
import sloc.readers.CSVReaderFactory;
import slope.Parsing;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
/**
 * A Storage element is something such as a single CVS file or a single String. It is used
 * by the StorageReader that stores multiple of such Elements.
 * 
 * @author Stefan Hanenberg (stefan.hanenberg@gmail.com)
 *
 * @param <USED_CLASS>
 */
public class CSVStorageElement<USED_CLASS> {
	public final CSVReaderFactory readerFactory;
	public final Header header;
	public final Class<USED_CLASS> clazz;

	public CSVStorageElement(CSVReaderFactory readerFactory,
			Class<USED_CLASS> clazz) {
		super();
		try {
			this.readerFactory = readerFactory;
			this.clazz = clazz;
			BufferedReader reader = createReader();
			String s = 	reader.readLine();
			this.header = createHeader(s);
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public BufferedReader createReader() {
//		System.out.println("the reader" + readerFactory.createReader());
		return new BufferedReader(readerFactory.createReader());
	}
	
	public BufferedReader createBodyReader() {
		BufferedReader r = createReader();
		IO.unsaveReadLine(r); // throw away first line which is for the header;
//		System.out.println(IO.unsaveReadLine(r)); // throw away first line which is for the header;
//		System.out.println("done");
		return r;
	}	

	private Header createHeader(String headerLine) {
		final ResultObject<Header> result = new ResultObject<Header>();

		CVSHeaderParserTokens tokens = new CVSHeaderParserTokens();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, headerLine);
		Rule parserRule = tokens.new CVSHeaderParser().START(result.setter);
		p.parseWithParseTreeConstruction(parserRule);
		return result.result;
	}

	private USED_CLASS createObject(String bodyLine) {
		USED_CLASS targetObject = Reflection.instantiateClassUnsave(clazz);
		header.parseLineInto(bodyLine, targetObject);
		return targetObject;
	}

	public List<USED_CLASS> getAll() {
		Condition<USED_CLASS> allTrue = new Condition<USED_CLASS>() {
			public boolean test(USED_CLASS t) {	return true; }};
		return getAll(allTrue);
	}

	public USED_CLASS getFirst(Condition<USED_CLASS> condition) {
		
		BufferedReader reader = createBodyReader();
		String s = IO.readLineUnsave(reader);

		while (s!=null) {
			USED_CLASS obj = createObject(s);
			if (condition.test(obj)) {
				IO.unsaveClose(reader);				
				return obj;
			}
			s = IO.readLineUnsave(reader);
		}
		
		throw new RuntimeException("Did not find object matching condition");

	}
	
	public List<USED_CLASS> getAll(Condition<USED_CLASS> condition) {
		ArrayList<USED_CLASS> ret = new ArrayList<USED_CLASS>();

		BufferedReader reader = createBodyReader();
		String s = IO.readLineUnsave(reader);

		while (s!=null) {
			USED_CLASS obj = createObject(s);
			if (condition.test(obj))
				ret.add(obj);
			s = IO.readLineUnsave(reader);
		}
		
		IO.unsaveClose(reader);
		return ret;
	}
	
}
