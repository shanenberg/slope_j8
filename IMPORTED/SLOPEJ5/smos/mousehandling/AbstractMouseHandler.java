package smos.mousehandling;

import shapuc.graphics.points.Point2D;
import smos.Context;
import smos.SMOSMachine;

public class AbstractMouseHandler {
	
	public static final String automataString = 
"		MouseHandler(start) " + 
"																					" +
"		start 			--->	ε/-/initialize-->initialized, 						" + 
"		initialized 	---> 	mouseDown/-/start-->dragOrClick,					" + 
"		dragOrClick 	---> 	mouseMove/withinBorderOrTimer/--->dragOrClick, 		" + 
"		dragOrClick 	---> 	mouseMove/!withinBorderOrTimer/--->dragging, 		" + 
"		dragOrClick 	--->	mouseUp/withinBorderOrTimer/--->clicked, 			" + 
"		dragOrClick	 	--->	mouseUp/!withinBorderOrTimer/--->dragged, 			" + 
"		clicked 		--->	ε/-/initialize-->initialized,						" + 
"		dragged 		--->	ε/-/initialize-->initialized, 						" + 
"		dragging 		--->	mouseResize/-/initialize-->resizing, 				" + 
"		resizing 		--->	mouseResize/-/initialize-->resizing, 				" + 
"		dragOrResize 	--->	mouseUp/startsResizing/initialize-->initialized. 	";

	public enum MESSAGES {mouseDown, mouseMove, mouseUp, mouseResize};
	public enum STATES {start, initialized, dragOrClick, clicked, dragged, dragging, resizing, dragOrResize};

	public SMOSMachine<STATES, MESSAGES> automata = 
			new SMOSMachine<STATES, MESSAGES>(automataString, STATES.class, MESSAGES.class, this); 
	
	public final long CLICK_TIME_BORDER;
	public final float MOVE_BORDER;
	
	public AbstractMouseHandler(long cLICK_TIME_BORDER, float mOVE_BORDER) {
		super();
		CLICK_TIME_BORDER = cLICK_TIME_BORDER;
		MOVE_BORDER = mOVE_BORDER;
	}

	long time = -1; // time stamp of first click
	Point2D startPosition = null; // startPosition of first click

	public void initialize(Context c) {
		time = -1;
		startPosition = null;
	}

	public void start(Context<AutomataMouseEvent> wrappedMouseEvent) {
		time = System.currentTimeMillis();
		startPosition = wrappedMouseEvent.getContent().getPosition();
	}

	public boolean withinBorderOrTimer(
			Context<AutomataMouseEvent> wrappedMouseEvent) {
		return withinTimeLimit()
				|| withinMoveBorder(wrappedMouseEvent.getContent());
	}

	public boolean withinTimeLimit() {
		return (System.currentTimeMillis() - time) < CLICK_TIME_BORDER;
	}

	public boolean withinMoveBorder(AutomataMouseEvent mouseEvent) {
		return mouseEvent.getPosition().distanceTo(startPosition) < MOVE_BORDER;
	}

	public void mouseMove(AutomataMouseEvent mouseEvent) {
		automata.invoke(MESSAGES.mouseMove, new Context<AutomataMouseEvent>(
				mouseEvent));
	}

	public void mouseDown(AutomataMouseEvent mouseEvent) {
		automata.invoke(MESSAGES.mouseDown, new Context<AutomataMouseEvent>(
				mouseEvent));
	}
		

}
