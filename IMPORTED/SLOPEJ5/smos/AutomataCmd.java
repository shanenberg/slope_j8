/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package smos;

import java.lang.reflect.Method;

import shapuc.util.Reflection;

public abstract class AutomataCmd {
	public abstract void doCommand(Context c);

	public static AutomataCmd createCommand(String messageName, final Object controller) {
		try {
			final Method m = controller.getClass().getMethod(messageName, new Class[] {Context.class});
			if (m.getReturnType().isAssignableFrom(Boolean.class)) throw new RuntimeException();
			return new AutomataCmd() {
				public void doCommand(Context c) {
					Reflection.invokeVoidMethodUnsave(m, controller, c);
				}
			};
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Cannot find public method in controller named " + messageName + " with parameter Context.");
		}
	
	}
	
	
}
