package slope.lib.grammar;

import slope.PC;
import slope.Parser;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.parsetree.*;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.lib.Lambda01.Lambda01Tokens;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.TokenReader;

/*
   SimpleGrammar
   Start: GrammarRules
   Rules:
	GrammarRules -> Rule+.
	GrammarRule -> NonTerminal "->" Expression. 
	ReturnCode -> "{" "^" (ObjectReturn | LAReturn) "}".
	ObjektReturn -> TypeName (propertyName ":" localVariable)+.
	LAReturn -> "LA" "(" identifier "," identifier "(" identifier identifier ")" ")".
	Expression -> SimpleExpression+ (ReturnCode)? (("|" (SimpleExpression)+ (ReturnCode)?)*. 
    SimpleExpression -> (BracketExpression | Terminal | NonTerminal) 
    BracketExpression -> "(" Expression ")".
 */

public class GrammarParsing extends Tokens {

	@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
	@Separator public TokenReader<?> NEWLINE() {	return lib.NEWLINE; }
	
	@Token public TokenReader<?> TOKEN_OR() { return lib.OR; }
	@Token public TokenReader<?> DOT() { return lib.DOT; }
	@Token public TokenReader<?> COLON() { return lib.COLON; }
	@Token public TokenReader<?> COMMA() { return lib.COMMA; }
	@Token public TokenReader<?> PLUS() { return lib.PLUS; }
	@Token public TokenReader<?> EQUALS() { return lib.EQUALS; }
	@Token public TokenReader<?> CARET() { return lib.CARET; }
	@Token public TokenReader<?> OBRACKET() { return lib.OPEN_BRACKET; }
	@Token public TokenReader<?> CBRACKET() { return lib.CLOSED_BRACKET; }
	@Token public TokenReader<?> OCURLY_BRACKET() { return lib.OPEN_CURELY_BRACKET; }
	@Token public TokenReader<?> CCURLY_BRACKET() { return lib.CLOSED_CURLY_BRACKET; }
	@Token public TokenReader<?> QUESTION_MARK() { return lib.QUESTION_MARK; }
	@Token public TokenReader<?> SHORT_ARROW() { return lib.KEYWORD_SHORT_ARROW; }
	@Token public TokenReader<String> JAVA_IDENTIFIER() {	return lib.JAVA_IDENTIFIER; }
	@Token public TokenReader<?> QUOTATION_MARK() { return lib.QUOTATION_MARK; }
	@Token public TokenReader<?> KLEENE() { return lib.MULT; }
	@Keyword public TokenReader<?> KEYWORD_LA() { return new KeywordScanner("LA"); }
	@Keyword public TokenReader<?> KEYWORD_TOKENS() { return new KeywordScanner("Tokens:"); }
	@Keyword public TokenReader<?> KEYWORD_Start() { return new KeywordScanner("Start:"); }
	@Keyword public TokenReader<?> KEYWORD_RULES() { return new KeywordScanner("Rules:"); }
	@Keyword public TokenReader<?> KEYWORD_SEPARATORS() { return new KeywordScanner("Separators:"); }
	
	
	public class Grammar01Parser extends Parser<GrammarParsing> {

		/*
		   	SimpleGrammar
   			Start: Rule
   			Rules:
		 */
		public Rule Grammar() { return new Rule() { public PC pc() {
			return 
				AND(
					TOKEN(JAVA_IDENTIFIER()),
					TOKEN(KEYWORD_Start()), TOKEN(JAVA_IDENTIFIER()),
					TOKEN(KEYWORD_RULES()),
					GrammarRules()
				);
		}};}		
		
		/*
		 	Rule -> NonTerminal "->" Expression (ReturnCode)?. 
		 */
		public Rule GrammarRules() { return new Rule() { public PC pc() {
			return 
				NFOLD(GrammarRule());
		}};}
		
		/*
			GrammarRule -> NonTerminal "->" Expression (ReturnCode)?. 
		*/
		public Rule GrammarRule() { return new Rule() { public PC pc() { 
			return 
				AND(
					NON_TERMINAL(),
					TOKEN("->"),
					EXPRESSION(),
					TOKEN('.')
				);
		}};}

		/**
		  	Expression -> SimpleExpression+ (("|" (SimpleExpression)+ )*. 
		 */
		public Rule EXPRESSION() { return new Rule() { public PC pc() { 
			return 
				WITH_OPTIONAL(
					WITH_OPTIONAL(
						NFOLD(SIMPLE_EXPRESSION()),
						RETURN_CODE()),
					NFOLD(
						AND(
							TOKEN('|'),
							WITH_OPTIONAL(
								NFOLD(SIMPLE_EXPRESSION()),
								RETURN_CODE())
						)
					)
				);
		}};}	
		
		/*
		 * 	SimpleExpression -> (BracketExpression | Terminal | NonTerminal) ("?" | "*")?.
		 */
		public Rule SIMPLE_EXPRESSION() { return new Rule() { public PC pc() { 
			return 
				WITH_OPTIONAL(
					OR(
						AND(VARBINDING(), SIMPLE_ATOMICEXPRESSION()),
						SIMPLE_ATOMICEXPRESSION()
					),
					OR(TOKEN('?'), TOKEN('*'), TOKEN('+'))
				);
		}};}			
	
		
		/*
		 * 	SimpleExpression -> (BracketExpression | Terminal | NonTerminal) ("?" | "*")?.
		 */
		public Rule SIMPLE_ATOMICEXPRESSION() { return new Rule() { public PC pc() { 
			return 
				OR(BRACKET_EXPRESSION(), TERMINAL(), NON_TERMINAL());
		}};}	
		
		/*
		 * 	SimpleExpression -> (BracketExpression | Terminal | NonTerminal) ("?" | "*")?.
		 */
		public Rule VARBINDING() { return new Rule() { public PC pc() { 
			return 
				AND(TOKEN(JAVA_IDENTIFIER()), TOKEN('='));
		}};}		

		/*
		 *  			BracketExpression -> "(" Expression ")".
		 */
		public Rule BRACKET_EXPRESSION() { return new Rule() { public PC pc() { 
			return 
				AND(
					TOKEN('('),
					EXPRESSION(),
					TOKEN(')')
				);
		}};}		

		
		public Rule NON_TERMINAL() { return new Rule() { public PC pc() { 
			return 
				TOKEN(JAVA_IDENTIFIER());
		}};}	
		
		
		/*
		 *  			Terminal -> "\"" Identifier "\"".
		 */		
		public Rule TERMINAL() { return new Rule() { public PC pc() { 
			return 
				AND(
					TOKEN('"'),
					TOKEN(JAVA_IDENTIFIER()),
					TOKEN('"')
				);
		}};}	

		/*
		ReturnCode -> "{" "^" (ObjectReturn | LAReturn) "}".
		
		*/
		public Rule RETURN_CODE() { return new Rule() { public PC pc() { 
			return 
				AND(
					TOKEN('{'),
					TOKEN('^'),
					OR(OBJECT_RETURN(), LA_RETURN()),
					TOKEN('}')
				);
		}};}	

/*
 	ObjektReturn -> TypeName (PropertyValue)+.
 */
		public Rule OBJECT_RETURN() { return new Rule() { public PC pc() { 
			return 
				AND(
					TOKEN(JAVA_IDENTIFIER()),
					NFOLD(PROPERTY_VALUE())
				);
		}};}		
				
		/*
 	PropertyValue -> propertyName ":" localVariable
	 */
		public Rule PROPERTY_VALUE() { return new Rule() { public PC pc() { 
			return 
				AND(
					TOKEN(JAVA_IDENTIFIER()),
					TOKEN(':'),
					TOKEN(JAVA_IDENTIFIER())
				);
		}};}	
		
		/*
			LAReturn -> "LA" "(" identifier "," identifier "(" identifier identifier ")" ")".		
	 	parseRule("AStupidRule -> a=anotherRule+ b=xRule {^LA(a, MyType(left,right)}.");
	 */
			public Rule LA_RETURN() { return new Rule() { public PC pc() { 
				return 
					AND(
						TOKEN("LA"), TOKEN('('), 
						TOKEN(JAVA_IDENTIFIER()),
						TOKEN(','),
						TOKEN(JAVA_IDENTIFIER()),
						TOKEN('('),
						TOKEN(JAVA_IDENTIFIER()),
						TOKEN(','),
						TOKEN(JAVA_IDENTIFIER()),
						TOKEN(')'),
						TOKEN(')')
					);
			}};}		

	}


	
//	
//	public Rule BOUNDING(final SetCmd<?> resultSetter) { return new Rule() { public PC pc() { 
//		return 
//			OR(
//				LOCAL_VARIABLE(null),
//				FIELD(null)
//			);
//	}};}
//	
//	public Rule LOCAL_VARIABLE(final SetCmd<?> resultSetter) { return new Rule() { public PC pc() { 
//		return 
//			AND(
//				TOKEN(JAVA_IDENTIFIER()),
//				TOKEN('=')
//			);
//	}};}		
//	
//	public Rule FIELD(final SetCmd<?> resultSetter) { return new Rule() { public PC pc() { 
//		return 
//			AND(
//				TOKEN(JAVA_IDENTIFIER()),
//				TOKEN(':')
//			);
//	}};}		

}
