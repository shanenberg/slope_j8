package slope.lib.grammar.parsetree;

public class GrammarRule {
	public String ruleName;
	public Expression expression;
	public ReturnCode returnCode;
}
