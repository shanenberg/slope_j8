package slope.lib.lambda.v01;

import slope.PC;
import slope.Parser;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slope.lang.SET.NULLParserConstructor;
import slope.lib.lambda.v01.parsetree.Abstraction;
import slope.lib.lambda.v01.parsetree.Application;
import slope.lib.lambda.v01.parsetree.BracketExpression;
import slope.lib.lambda.v01.parsetree.LambdaExpression;
import slope.lib.lambda.v01.parsetree.Variable;
import slowscane.lib.Lambda01.Lambda01Tokens;

public class Lambda01ParserTokens extends Lambda01Tokens {

	public class Lambda01Parser extends Parser<Lambda01ParserTokens> {
		
		/*
		 LambdaExpression -> BracketExpression | Variable | Abstraction | Application
		 */
		public Rule LAMBDA_EXPRESSION(final SetCmd<LambdaExpression> resultSetter) { return new Rule() { public PC pc() { 
			return 
				OR(
					BRACKET_EXPRESSION(resultSetter),
					VARIABLE(resultSetter),
					ABSTRACTION(resultSetter),
					APPLICATION(resultSetter)
				);
		}};}

//		@Node(BracketExpression)
		public Rule BRACKET_EXPRESSION(final SetCmd<LambdaExpression> resultSetter) { return new Rule() { public PC pc() { 

			final BracketExpression be = new BracketExpression();

			final SetCmd<LambdaExpression> setInner = new SetCmd<LambdaExpression>() { public void set(LambdaExpression value) {
					be.childExpression = value;
				}
			};
			
			return 
				AND(
					TOKEN('('), LAMBDA_EXPRESSION(setInner), TOKEN(')'), result(resultSetter, be)
				);
		}};}
		
		public Rule VARIABLE(final SetCmd<LambdaExpression> resultSetter) { return new Rule() { public PC pc() { 
			
			final Variable variable = new Variable();
			SetCmd<String> setCmd = new SetCmd<String>() { public void set(String value) {
					variable.varName = value;
					resultSetter.set(variable);
				}
			};

			return 
				TOKEN(JAVA_IDENTIFIER(), setCmd);
		}};}			
		
		
		public Rule ABSTRACTION(final SetCmd<LambdaExpression> resultSetter) { return new Rule() { public PC pc() { 
			
			final Abstraction abst = new Abstraction();

			final SetCmd<String> setHead = new SetCmd<String>() { public void set(String value) {
				abst.varName = value;
			}};
			
			final SetCmd<LambdaExpression> setBody = new SetCmd<LambdaExpression>() {
				public void set(LambdaExpression value) {
					abst.body = value;
			}};
			
			return 
				AND(
					TOKEN('('), TOKEN('λ'), 
					TOKEN(JAVA_IDENTIFIER(), setHead), 
					TOKEN('.'), LAMBDA_EXPRESSION(setBody), TOKEN(')'),
					result(resultSetter, abst)
				);
		}};}			
		
		public Rule APPLICATION(final SetCmd<LambdaExpression> resultSetter) { return new Rule() { public PC pc() { 
			
			final Application appl = new Application();

			final SetCmd<LambdaExpression> setLeft = new SetCmd<LambdaExpression>() { public void set(LambdaExpression le) {
				appl.left = le;
			}};
			
			final SetCmd<LambdaExpression> setRight = new SetCmd<LambdaExpression>() { public void set(LambdaExpression le) {
				appl.right = le;
			}};			
			
			return 
				AND(
					TOKEN('('), LAMBDA_EXPRESSION(setLeft), LAMBDA_EXPRESSION(setRight), TOKEN(')'), result(resultSetter, appl)
				);
		}

		};}	
	
	}	
}