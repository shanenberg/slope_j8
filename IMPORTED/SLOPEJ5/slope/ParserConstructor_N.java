/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import shapuc.util.Reflection;

public class ParserConstructor_N <PARSER_TYPE extends ElementParser> implements PC {

	public final Class<PARSER_TYPE> parserType;
	public final PC[] pcs;
	public final String pcTypeName;

	public ParserConstructor_N(String typeName, Class<PARSER_TYPE> treeNodeType, PC[] pcs) {
		super();
		this.parserType = treeNodeType;
		this.pcs = pcs;
		this.pcTypeName = typeName;
	}
	
	/** Creates the parser instance using Reflection */
	public PARSER_TYPE createParser() {
		return (PARSER_TYPE) Reflection.instantiateClassUnsave(parserType, pcs);
	}
	
	public List<PC> getPCChildren() {
		return Arrays.asList(pcs);
	}
	
	public String getPCTypeName() {
		return pcTypeName;
	}

}
