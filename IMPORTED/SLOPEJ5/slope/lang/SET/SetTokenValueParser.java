/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.lang.SET;

import slope.ElementParser;
import slope.SetCmd;
import slope.lang.TOKEN.TokenParserConstructor;
import slope.lang.TOKEN.TokenTreeNode;
import slowscane.streams.TokenStream;

public class SetTokenValueParser<TOKEN_TYPE> extends ElementParser<SetTokenValueTreeNode<TOKEN_TYPE>> {
	
	public final TokenParserConstructor<TOKEN_TYPE> parserConstructor;
	final SetCmd<TOKEN_TYPE> setObject;
	
	public SetTokenValueParser(TokenParserConstructor<TOKEN_TYPE> parserConstructor, SetCmd<TOKEN_TYPE> setObject) {
		super("SETVal");
		this.parserConstructor = parserConstructor;
		this.setObject = setObject;
	}

	public SetTokenValueTreeNode<TOKEN_TYPE> parse(TokenStream<?> tokenstream) {
		TokenTreeNode<TOKEN_TYPE> treeNode = parserConstructor.createParser().parse(tokenstream);
		return new SetTokenValueTreeNode<TOKEN_TYPE>(setObject, treeNode);
	}
}
