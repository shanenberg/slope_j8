package shapuc.graphics.points;

/*
 * Class for 2-dimensional double-Points
 */
public class Point2D {
	public double x;
	public double y;

	public static Point2D xy(double x, double y) {
		return new Point2D(x, y);
	}

	public Point2D(double _x, double _y) {
		x = _x;
		y = _y;
	}

	public Point2D clone() {
		return new Point2D(x, y);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point2D other = (Point2D) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

	public Point2D scale(double s) {
		x = s * x;
		y = s * y;
		return this;
	}

	public Point2D scaleNCopy(double s) {
		return new Point2D(x * s, y * s);
	}

	public double getScaleXToFitCompletelyInto(double xFrame) {
		return xFrame / x;
	}

	public double getScaleYToFitCompletelyInto(double yFrame) {
		return yFrame / y;
	}

	public double getScaleToFitCompletelyInto(Point2D p) {
		double xScale = getScaleXToFitCompletelyInto(p.x);
		double yScale = getScaleYToFitCompletelyInto(p.y);
		return Math.min(xScale, yScale);
	}

	public double getScaleToFitBorderedInto(Point2D p) {
		double xScale = getScaleXToFitCompletelyInto(p.x);
		double yScale = getScaleYToFitCompletelyInto(p.y);
		return Math.max(xScale, yScale);
	}

	public Point2D centralizationOffset(Point2D p) {
		return new Point2D((p.x - x) / 2f, (p.y - y) / 2f);
	}

	public Point2D plus(Point2D toAdd) {
		x = x + toAdd.x;
		y = y + toAdd.y;
		return this;
	}

	public Point2D plusCopy(Point2D toAdd) {
		return new Point2D(x + toAdd.x, y + toAdd.y);
	}

	public Point2D minus(Point2D minus) {
		x = x - minus.x;
		y = y - minus.y;
		return this;
	}

	public Point2D minusCopy(Point2D minus) {
		return new Point2D(x - minus.x, y - minus.y);
	}

	@Override
	public String toString() {
		return super.toString() + "...x:" + x + "...y:" + y;
	}

	public boolean widerThan(Point2D frame) {
		return x >= frame.x;
	}

	public boolean higherThan(Point2D frame) {
		return y >= frame.y;
	}

	public double distanceTo(Point2D p) {
		double x = Math.abs(this.x - p.x);
		double y = Math.abs(this.y - p.y);

		return Math.sqrt((x * x) + (y * y));

	}

}
