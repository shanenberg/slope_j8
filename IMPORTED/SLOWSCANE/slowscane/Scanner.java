/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slowscane;

import java.util.List;

import slowscane.scanners.TokenReader;
import slowscane.streams.PositionStream;
import slowscane.streams.TokenStream;

/* 
 * TOKEN_TYPES is the interface that contains the declaration of tokens.
*/
public class Scanner<TOKEN_TYPES extends Tokens, POSITIONSTREAM extends PositionStream<?>> {

	TOKEN_TYPES tokentypes;
	
	POSITIONSTREAM stream;

	List<TokenReader<?>> scanners;
	List<TokenReader<?>> skippedElementScanners;
	
	public Scanner(TOKEN_TYPES tokenTypes, POSITIONSTREAM stream) {
		super();
		this.tokentypes = tokenTypes;
		scanners = tokentypes.getAllTokenTypes();
		skippedElementScanners = tokentypes.getSkippedTokenTypes();
		this.stream = stream;
	}

	public TOKEN_TYPES getTokenTypes() {
		return tokentypes;
	}
	
	public TokenStream scan() {
		TokenStream  ret = new TokenStream(stream);
		
		removeSkippedElements();
		while (!stream.isEOF()) {
			boolean madeProgress = false;
			
			for (TokenReader<?> scanner: scanners) {
				Token<?> t = saveScan(scanner);
				if (t !=null) {
					ret.add(t);
					madeProgress = true;
					removeSkippedElements();
					break;
				}
			}
			
			// If I did not make any progress, there must be an error
			if (!madeProgress) {
				throw new ScanException("No progress here", stream.stuckExceptionString());
			}
		}
		if (!stream.isEOF())
			throw new ScanException("Not reached End of File", stream.stuckExceptionString());
		return ret;
	}

	/**
	 * Removes elements that can be skipped
	 */
	private void removeSkippedElements() {
		boolean isSkipping = true;
		while (isSkipping) {
			isSkipping = false;
			for (TokenReader<?> elementScanner : skippedElementScanners) {
				if (saveScan(elementScanner)!=null)
					isSkipping=true;
			}
		}
	}

	private Token<?> saveScan(TokenReader<?> scanner) {
		int streamPosition = stream.currentPosition;
		try {
			return scanner.scanNext(stream);
		} catch (Exception e) {
			stream.currentPosition = streamPosition;
		}
		return null;
	}
	
	
}
