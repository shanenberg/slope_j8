/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slowscane.lib;

import slowscane.Scanner;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;
import slowscane.streams.StringBufferStream;
import slowscane.streams.TokenStream;

public class CSV extends ExamplesTestCase {

	public static class CSVHeaderTokens extends Tokens {
		@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
		@Separator public TokenReader<?> TAB() {	return lib.TAB; 	}
		
		@Token public TokenReader<?> COLON() { return lib.COLON; }
		@Token public TokenReader<?> OPEN_BRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> OPEN_CURELY_BRACKET() { return lib.OPEN_CURELY_BRACKET; }
		@Token public TokenReader<?> CLOSED_BRACKET() { return lib.CLOSED_BRACKET; }
		@Token public TokenReader<?> CLOSED_CURELY_BRACKET() { return lib.CLOSED_CURLY_BRACKET; }
		
		@Keyword public TokenReader<String> INT() { return lib.KEYWORD_Int; }
		@Keyword public TokenReader<String> HEXA() { return lib.KEYWORD_Hexa; }
		@Keyword public TokenReader<String> LIST() { return lib.KEYWORD_List; }
		@Keyword public TokenReader<String> NOTNULL() { return lib.KEYWORD_NotNull; }
		@Keyword public TokenReader<String> REF() { return lib.KEYWORD_Ref; }
		@Keyword public TokenReader<String> STRING() { return lib.KEYWORD_String; }
		@Keyword public TokenReader<String> UNIQUE() { return lib.KEYWORD_Unique; }
		@Keyword public TokenReader<String> MAP() { return lib.KEYWORD_Map; }
		

		@Token public TokenReader<String> IDENTIFIER() { return lib.JAVA_IDENTIFIER; }
    }
	
	public static class CSVBodyTokens extends Tokens {
		@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
		@Separator public TokenReader<?> TAB() {	return lib.TAB; 	}
		
		@Token public TokenReader<?> OPEN_CURELY_BRACKET() { return lib.OPEN_CURELY_BRACKET; }
		@Token public TokenReader<?> CLOSED_CURELY_BRACKET() { return lib.CLOSED_CURLY_BRACKET; }
		
		@Token public TokenReader<?> OPEN_BRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> CLOSED_BRACKET() { return lib.CLOSED_BRACKET; }

		@Token public TokenReader<?> COLON() { return lib.COLON; }

		@Keyword public TokenReader<String> NULL() { return lib.KEYWORD_NULL; }
		@Token public TokenReader<?> COMMA() { return lib.COMMA; }

		@Token public TokenReader<String> STRING() { return lib.JAVA_STRING_LITERAL; }
		@Token public TokenReader<Integer> INTEGER() { return lib.JAVA_INTEGER_LITERAL; }
		@Token public TokenReader<Integer> HEXAINT() { return lib.JAVA_HEXAINT_LITERAL; }
    }	

	public void test01() {
		StringBufferStream stream = new StringBufferStream("id:Int text:String");

		Scanner<CSVHeaderTokens, StringBufferStream> 
			scanner = new Scanner<CSVHeaderTokens, StringBufferStream>(new CSVHeaderTokens(), stream);
		
		TokenStream<?> res = scanner.scan();
		assertEquals(6, res.length());
		
		
	}
	
}

