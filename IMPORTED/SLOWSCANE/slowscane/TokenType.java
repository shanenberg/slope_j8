package slowscane;

public class TokenType {
	public TokenType(String tokenTypeName) {
		super();
		this.tokenTypeName = tokenTypeName;
	}

	private String tokenTypeName;

	public String getTokenTypeName() {
		return tokenTypeName;
	}

	public void setTokenTypeName(String tokenTypeName) {
		this.tokenTypeName = tokenTypeName;
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof TokenType))
			return false;
		TokenType thatType = (TokenType) obj;

		return thatType.getTokenTypeName().equals(this.getTokenTypeName());
	
	}

	@Override
	public String toString() {
		return "TokenType [tokenTypeName=" + tokenTypeName + "]";
	}
	
	
	
	
}
