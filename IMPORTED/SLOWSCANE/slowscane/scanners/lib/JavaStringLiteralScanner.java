/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slowscane.scanners.lib;

import slowscane.ScanException;
import slowscane.Token;
import slowscane.TokenType;
import slowscane.scanners.TokenReader;
import slowscane.streams.PositionStream;


public class JavaStringLiteralScanner extends TokenReader<String> {

	{ this.tokenType = new TokenType("SLOPE_StringLiteral"); }
	
	@Override
	public Token<String> scanNext(PositionStream<?> stream) {
		StringBuffer ret = new StringBuffer();
		Character next = (Character) stream.next();

		if (next!='"')
			throw new ScanException("No Java String Literal", stream.stuckExceptionString());

//		ret.append(next);

		while (!stream.isEOF()) {
			next = (Character) stream.next();
			
			if (isEscapeChar(next)) {
				next = (Character) stream.next();
				if (next=='t')
					ret.append("\t");
				else if (next == '\"')
					ret.append("\"");
				else if (next == 'n')
					ret.append("\n");
			} else {
				if (next=='\"') {
					return createToken(stream, ret.toString());
				} else {
					ret.append(next);					
				}
			}
		}
		throw new ScanException("No Java String Literal", stream.stuckExceptionString());
	}
	
	private boolean isEscapeChar(Character next) {
		return next == '\\';
	}

}
