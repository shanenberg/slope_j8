/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slowscane.scanners.lib;

import slowscane.streams.PositionStream;

/**
 * Reads a Float literal, which is for example "1.00123", but not "01,00" (because of the leading "0")). Note, that
 * this is not a Java float, i.e. "1.2f" is not scanned by this Scanner.
 * 
 * @author Stefan Hanenberg (stefan.hanenberg@gmail.com)
 *
 */
public class FloatLiteralScanner extends NumberLiteralScanner<Float> {
	@Override
	public Float convertStringToNumber(String s) {
		return Float.valueOf(s);
	}
	
	protected String readNumberStringFromStream(PositionStream<?> stream) {
		StringBuffer ret = new StringBuffer(super.readNumberStringFromStream(stream));

		if (stream.isEOF()) return ret.toString();
		
		Character next=(Character) stream.next();
		if (!next.equals(new Character('.'))) {
			stream.currentPosition--;
			return ret.toString();
		}
		while (!stream.isEOF() && isDigit(next = (Character) stream.next())) {
			ret.append(next);
		}

		if (!isDigit(next))
				stream.currentPosition--;
		return ret.toString();
	}	


}
