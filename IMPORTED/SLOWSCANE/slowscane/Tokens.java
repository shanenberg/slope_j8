/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slowscane;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import shapuc.util.Reflection;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;

public class Tokens {
	public TokensLib lib = new TokensLib(); 
	
	/**
	 * Reads the TokenTypes from the Tokens. 
	 * 
	 * A TokenType is a method that has the annotation @TokenType and has the return type @ElementScanner
	 * 
	 * @return
	 */
	
	public List<TokenReader<?>> getAllTokenTypes() {
		ArrayList<TokenReader<?>> ret = new ArrayList<TokenReader<?>>();
		ret.addAll(this.getKeywordTokenTypes());
		ret.addAll(this.getTokenTypes());
		return ret;
	}	
	
	public List<TokenReader<?>> getKeywordTokenTypes() {
		ArrayList<TokenReader<?>> ret = new ArrayList<TokenReader<?>>();
		Method[] methods = this.getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			if (fulfillsKeywordConstraints(methods[i])) {
				TokenReader<?> s = Reflection.<TokenReader<?>>invokeMethodUnsave(methods[i], this);
				ret.add(s);
			}
		}
		return ret;
	}	

	/**
	 * Reads the TokenTypes from the Tokens. 
	 * 
	 * A TokenType is a method that is annotation with @TokenType and has the return type @ElementScanner
	 * 
	 * @return
	 */
	public List<TokenReader<?>> getTokenTypes() {
		ArrayList<TokenReader<?>> ret = new ArrayList<TokenReader<?>>();
		Method[] methods = this.getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			if (fulfillsTokenConstraints(methods[i])) {
				TokenReader<?> tokenReader = Reflection.<TokenReader<?>>invokeMethodUnsave(methods[i], this);
				ret.add(tokenReader);
			}
		}
		return ret;
	}
	
	public List<TokenReader<?>> getSkippedTokenTypes() {
		ArrayList<TokenReader<?>> ret = new ArrayList<TokenReader<?>>();
		Method[] methods = this.getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			if (fulfillsSkippedTokenConstraints(methods[i])) {
				ret.add( Reflection.<TokenReader<?>>invokeMethodUnsave(methods[i], this));
			}
		}
		return ret;
	}	
	
	private boolean fulfillsTokenConstraints(Method method) {
		return hasAnnotationType(Token.class, method) && 
//				method.getModifiers()==Modifier.PUBLIC &&
				TokenReader.class.isAssignableFrom(method.getReturnType());
	}

	private boolean fulfillsSkippedTokenConstraints(Method method) {
		return hasAnnotationType(Separator.class, method) && 
//				method.getModifiers()==Modifier.PUBLIC &&
				TokenReader.class.isAssignableFrom(method.getReturnType());
	}
	
	
	
	private boolean fulfillsKeywordConstraints(Method method) {
		return hasAnnotationType(Keyword.class, method) && 
//				method.getModifiers()==Modifier.PUBLIC &&
				TokenReader.class.isAssignableFrom(method.getReturnType());
	}
	
	private boolean hasAnnotationType(Class<?> annotationClass, Method method) {
		Annotation[] annotations = method.getAnnotations();
		
		for (int j = 0; j < annotations.length; j++) {
			if (annotations[j].annotationType()==annotationClass) {
				return true;
			}
		}
		return false;
	}
}
