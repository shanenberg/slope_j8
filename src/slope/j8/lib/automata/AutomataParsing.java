package slope.j8.lib.automata;
/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

 
import junit.framework.TestCase;
import slope.Parser;
import slope.Parsing;
import slope.j8.Rule;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;

public class AutomataParsing extends TestCase {

	public static class AutomataParsingTokens extends Tokens {
		
		// Whitespaces are separators but not used while parsing (i.e. they are skipped)
		@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
		@Separator public TokenReader<?> NEWLINE() {	return lib.NEWLINE; 	}
		
		@Keyword public TokenReader<String> PACKAGE_KEYWORD() { return lib.KEYWORD_PACKAGE;}
		@Keyword public TokenReader<?> ARROW() { return lib.KEYWORD_ARROW;}
		
		@Token public TokenReader<?> SLASH() { return lib.DIV;}
		@Token public TokenReader<?> MINUS() { return lib.MINUS;}
		@Token public TokenReader<?> EPSILON() { return lib.EPSILON;}
		@Token public TokenReader<?> SEMICOLON() { return lib.SEMICOLON;}
		@Token public TokenReader<?> COMMA() { return lib.COMMA;}
		@Token public TokenReader<?> IDENTIFIER() { return lib.JAVA_IDENTIFIER;}
		@Token public TokenReader<?> DOT() { return lib.DOT;}
		@Token public TokenReader<?> OPEN_BRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> CLOSED_BRACKET() { return lib.CLOSED_BRACKET; }
		
		public class AutomataParser extends Parser<AutomataParsingTokens> {

			Rule START() { 
				return () ->
				AND(
				  PACKAGE(),
				  AUTOMATA_NAME_START(),
				  RULES(), TOKEN('.')
				);
			}

			Rule PACKAGE() { return () ->
			  AND(TOKEN(PACKAGE_KEYWORD()), PACKAGE_NAME(), TOKEN(';'));
			}
			
			Rule AUTOMATA_NAME_START() { return () ->
				AND(TOKEN(IDENTIFIER()), TOKEN('('), TOKEN(IDENTIFIER()), TOKEN(')'));
			}
			
			Rule RULES() { return () ->
			  OR(
			    AND(RULE(), NFOLD(AND(TOKEN(','), RULE()))),
			    RULE()
			  );
			}
						
			Rule RULE() { return () ->
				AND(
				  TOKEN(IDENTIFIER()), 
				  TOKEN(ARROW()), 
				  TRANSITION(),
				  TOKEN(ARROW()), 
				  NEXT_STATE()
				);
			}
			/*
			RULE<Rule> RULE() {
			    Rule r = new Rule();
			    
 				return () ->			    
				AND(
				  TOKEN(IDENTIFIER(), (String s)-> r.name = s), 
				  TOKEN(ARROW()), 
				  TRANSITION((Transition t)-> r.transition=t),
				  TOKEN(ARROW()), 
				  NEXT_STATE()...., (String nextState) -> r.nextState = nextState
				);
			}			 
			 */
			
			Rule TRANSITION() { return () ->
			  AND(
			    IN_MSG(), TOKEN('/'),
			    IN_CONDITION(), TOKEN('/'),
			    OUT_STATEMENT()
			  );
			}
			
			Rule IN_MSG() { return () ->
			  OR(
			    TOKEN('ε'),
			    TOKEN(IDENTIFIER())
			  );
			}
			
			Rule IN_CONDITION() {return () ->
			  OR(TOKEN('-'), TOKEN(IDENTIFIER()));
			}
			
			Rule OUT_STATEMENT() {return () ->
			  OR(TOKEN('-'), TOKEN(IDENTIFIER()));
			}			
			
			Rule NEXT_STATE() {return () ->
			  OR(
			    AND(TOKEN(IDENTIFIER()), TOKEN('/'), TOKEN(IDENTIFIER())),
			    TOKEN(IDENTIFIER())
			  );
			}			
			
			
			Rule PACKAGE_NAME() { return () ->
			  OR(
				AND(TOKEN(IDENTIFIER()), NFOLD(AND(TOKEN('.'), TOKEN(IDENTIFIER())))),
				TOKEN(IDENTIFIER())
			  );
			}
			/* 
			RULE PACKAGE_NAME() { return () -> {
			  StringBuffer ret = new StringBuffer();
			  RULE packageNameTreeNode = super.PACKAGE_NAME();
			  
			  
			*/
		}
    }		
		
	@SuppressWarnings("unchecked")
	public void checkPackage(String s) {
		AutomataParsingTokens tokens = new AutomataParsingTokens();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		p.parse(tokens.new AutomataParser().PACKAGE());
	}

	public void checkNameNStart(String s) {
		AutomataParsingTokens tokens = new AutomataParsingTokens();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		p.parse(tokens.new AutomataParser().AUTOMATA_NAME_START());
	}
	
	public void checkRule(String s) {
		AutomataParsingTokens tokens = new AutomataParsingTokens();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		p.parse(tokens.new AutomataParser().RULE());
	}	
	
	public void checkTransition(String s) {
		AutomataParsingTokens tokens = new AutomataParsingTokens();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		p.parse(tokens.new AutomataParser().TRANSITION());
	}		
	
	public void check(String s) {
		AutomataParsingTokens tokens = new AutomataParsingTokens();
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		p.parse(tokens.new AutomataParser().START());
	}	
	
	public void test01() {
		checkPackage("package smaug.lib.qagame.gen;");
		checkNameNStart("QAGameAutomata(automataStart)\n");
		checkTransition("ε/-/initialize");
		checkRule("automataStart --->  ε/-/initialize ---> initialized");
		check("package smaug.lib.qagame.gen;  "
		  + "QAGameAutomata(automataStart)\n"
		  + "  automataStart --->  ε/-/initialize ---> initialized.");
		
		check("package smaug.lib.qagame.gen;  "
				  + "QAGameAutomata(automataStart)\n"
				  + "  automataStart --->  ε/-/initialize ---> initialized,\n"
				  + "  initialized --->  ε/-/- ---> createNewQuestion,\n"
				  + "  createNewQuestion --->  ε/-/setNextQuestion ---> askingQuestion.");
		
	}
}
