package slope.j8.lib.automata;
/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/


import junit.framework.TestCase;
import slope.Parser;
import slope.Parsing;
import slope.j8.Rule;
import slope.SetCmd;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;

public class AutomataParseTree extends TestCase {

	public static class AutomataParsingTokens extends Tokens {
		
		// Whitespaces are separators but not used while parsing (i.e. they are skipped)
		@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
		@Separator public TokenReader<?> NEWLINE() {	return lib.NEWLINE; 	}
		
		@Keyword public TokenReader<String> PACKAGE_KEYWORD() { return lib.KEYWORD_PACKAGE;}
		@Keyword public TokenReader<?> ARROW() { return lib.KEYWORD_ARROW;}
		
		@Token public TokenReader<?> SLASH() { return lib.DIV;}
		@Token public TokenReader<?> MINUS() { return lib.MINUS;}
		@Token public TokenReader<?> EPSILON() { return lib.EPSILON;}
		@Token public TokenReader<?> SEMICOLON() { return lib.SEMICOLON;}
		@Token public TokenReader<?> COMMA() { return lib.COMMA;}
		@Token public TokenReader<String> IDENTIFIER() { return lib.JAVA_IDENTIFIER;}
		@Token public TokenReader<?> DOT() { return lib.DOT;}
		@Token public TokenReader<?> OPEN_BRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> CLOSED_BRACKET() { return lib.CLOSED_BRACKET; }
		
		public class AutomataParser extends Parser<AutomataParsingTokens> {

			/*
			 * <START> -> <PACKAGE> <AUTOMATA_NAME_START> <RULES>  
			 */
			Rule START(SetCmd<Automata> result) { 
				final Automata automata = new Automata();
				return () ->
				  AND(
				    PACKAGE(automata),
				    AUTOMATA_NAME_START(automata),
				    RULES(automata), TOKEN('.'),
				    set(()->result.set(automata))
				  );
			}

			Rule PACKAGE(Automata automata) { return () ->
			  AND(TOKEN(PACKAGE_KEYWORD()), PACKAGE_NAME(automata), TOKEN(';'));
			}
			
			Rule AUTOMATA_NAME_START(Automata automata) { 
				return () ->
				  AND(
					TOKEN(IDENTIFIER(), (String s)-> automata.name = s), 
					TOKEN('('), 
					TOKEN(IDENTIFIER(), (String s)-> automata.startRuleName = s), 
					TOKEN(')'));
			}
			
			Rule RULES(Automata automata) { return () ->
			  OR(
			    AND(RULE(automata), NFOLD(AND(TOKEN(','), RULE(automata)))),
			    RULE(automata)
			  );
			}
						
			Rule RULE(Automata automata) { 
				
				Transition transition = new Transition();
				
				return () ->
				AND(
				  TOKEN(IDENTIFIER(), (String s)-> transition.startStateName = s), 
				  TOKEN(ARROW()), 
				  TRANSITION(transition),
				  TOKEN(ARROW()), 
				  NEXT_STATE(transition),
				  set(()->automata.transitions.add(transition))
				);
			}
			
			Rule TRANSITION(Transition transition) { return () ->
			  AND(
			    IN_MSG(transition), TOKEN('/'),
			    IN_CONDITION(transition), TOKEN('/'),
			    OUT_STATEMENT(transition)
			  );
			}
			
			Rule IN_MSG(Transition transition) { return () ->
			  OR(
			    TOKEN('ε', (Character c) -> transition.inMsg="ε"),
			    TOKEN(IDENTIFIER(), (String s) -> transition.inMsg=s)
			  );
			}
			
			Rule IN_CONDITION(Transition transition) {return () ->
			  OR(
			    TOKEN('-', (Character s) -> transition.conditionMsg = "-"), 
			    TOKEN(IDENTIFIER(), (String s) -> transition.conditionMsg = s));
			}
			
			Rule OUT_STATEMENT(Transition transition) {return () ->
			  OR(
			    TOKEN('-', (Character s) -> transition.outMsg = "-"), 
			    TOKEN(IDENTIFIER(), (String s) -> transition.outMsg = s));
			}			
			
			Rule NEXT_STATE(Transition transition) {return () ->
			  OR(
			    AND(
			      TOKEN(IDENTIFIER(), (String s)-> transition.nextStateName = s), 
			      TOKEN('/'), 
			      TOKEN(IDENTIFIER(), (String s)-> transition.nextStateCmdName = s)),
			    TOKEN(IDENTIFIER(), (String s)-> transition.nextStateName = s)
			  );
			}			
			
			
			Rule PACKAGE_NAME(Automata automata) { 
			  final StringBuffer stringBuffer = new StringBuffer();
			  SetCmd<String> append = (String s) -> stringBuffer.append(s);
			  SetCmd<Character> addPoint = (Character s) -> stringBuffer.append(".");
			  
			  return () ->
			  AND(
			    OR(
				  AND(
				    TOKEN(IDENTIFIER(), append), 
				    NFOLD(
				      AND(
				        TOKEN('.', addPoint), 
				        TOKEN(IDENTIFIER(), append)))),
				  TOKEN(IDENTIFIER(), append)
			    ),
                set(()->automata.packageName = stringBuffer.toString())
              );
			    
			}
			/* 
			RULE PACKAGE_NAME() { return () -> {
			  StringBuffer ret = new StringBuffer();
			  RULE packageNameTreeNode = super.PACKAGE_NAME();
			  
			  
			*/
		}
    }		
		

	public Automata check(String s) {
		AutomataParsingTokens tokens = new AutomataParsingTokens();
		class Result {public Automata automata;}
		Result result = new Result();
		
		SetCmd<Automata> resultSetter = new SetCmd<Automata>() {
			public void set(Automata value) {result.automata = value;}
		};
		
		@SuppressWarnings("rawtypes")
		Parsing p = new Parsing(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new AutomataParser().START(resultSetter));
		return result.automata;
	}	
	
	public void test01() {
		Automata a1 = check("package smaug.lib.qagame.gen;  "
		  + "QAGameAutomata(automataStart)\n"
		  + "  automataStart --->  ε/-/initialize ---> initialized.");
		
		assertEquals("QAGameAutomata", a1.name);
		assertEquals("smaug.lib.qagame.gen", a1.packageName);
		assertEquals("automataStart", a1.startRuleName);
		assertEquals(1, a1.transitions.size());
		assertEquals("automataStart", a1.transitions.get(0).startStateName);
		assertEquals("ε", a1.transitions.get(0).inMsg);
		assertEquals("-", a1.transitions.get(0).conditionMsg);
		assertEquals("initialize", a1.transitions.get(0).outMsg);
		assertEquals("initialized", a1.transitions.get(0).nextStateName);
		
		
		
		check("package smaug.lib.qagame.gen;  "
				  + "QAGameAutomata(automataStart)\n"
				  + "  automataStart --->  ε/-/initialize ---> initialized,\n"
				  + "  initialized --->  ε/-/- ---> createNewQuestion,\n"
				  + "  createNewQuestion --->  ε/-/setNextQuestion ---> askingQuestion.");
		
	}
}
