/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.j8.lib.toyexamples;

import java.util.ArrayList;

import junit.framework.TestCase;
import slope.PC;
import slope.Parser;
import slope.Parsing;
import slope.TreeNode;
import slope.j8.Rule;
import slowscane.Scanner;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;
import slowscane.streams.StringBufferStream;
import slowscane.streams.TokenStream;

public class TestLangParsing extends TestCase {

	public static class TestLangTokens extends slowscane.Tokens {
		@Separator public TokenReader<?> WHITESPACE() { return lib.WHITESPACE; }
		@Separator public TokenReader<?> NEWLINE() { return lib.NEWLINE;}
		@Keyword public TokenReader<String> Int() { return lib.KEYWORD_Int; }
		@Token public TokenReader<String> JAVA_IDENTIFIER() { return lib.JAVA_IDENTIFIER; }
		@Token public TokenReader<?> OBRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> CBRACKET() { return lib.CLOSED_BRACKET; }
		
		public class TestLangParser extends Parser<TestLangTokens> {
			
			Rule KEYWORD_Int() { return () -> 
				TOKEN(Int());
			}
			
			Rule IDENTIFIER() { return () ->
				TOKEN(JAVA_IDENTIFIER());
			}
			
			Rule EXPRESSION() { return () ->
				OR(KEYWORD_Int(), IDENTIFIER(), N_TIMES_BRACKET_EXPRESSION());
			}
			
			Rule BRACKET_EXPRESSION() { return () ->
				AND(TOKEN(OBRACKET()), EXPRESSION(), TOKEN(CBRACKET()));
			}
			
			Rule N_TIMES_BRACKET_EXPRESSION() { return () ->
				NFOLD(BRACKET_EXPRESSION());
			}
			
		}
		
	}

	public TokenStream<?> scan(String inputString) {
        Tokens tokens = new TestLangTokens();
        StringBufferStream stringBufferStream = new StringBufferStream(inputString);
        Scanner<?, StringBufferStream> scanner = 
            new Scanner<Tokens, StringBufferStream>(tokens, stringBufferStream);
        return scanner.scan();		
    }
	
	public void testScan() {
		TokenStream<?> ts = scan("(   \n (     (Int    )     )");
		assertEquals("(", ts.get(0).getValue().toString());
		assertEquals("(", ts.get(1).getValue().toString());
		assertEquals("(", ts.get(2).getValue().toString());
		assertEquals("Int", ts.get(3).getValue().toString());
	}
	
	public TreeNode check(String s) {
		TestLangTokens tokens = new TestLangTokens();
		Parsing p = new Parsing(tokens, s); 
		return p.parse(tokens.new TestLangParser().EXPRESSION());
	}
	
	public void testParse() {
		  check("(   (Int) )\n   (abc)   ( ((def)))");
	} 
	
	

}