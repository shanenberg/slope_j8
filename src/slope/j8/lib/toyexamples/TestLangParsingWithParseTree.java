/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package slope.j8.lib.toyexamples;

import java.util.ArrayList;

import junit.framework.TestCase;
import slope.PC;
import slope.Parser;
import slope.Parsing;
import slope.SetCmd;
import slope.TreeNode;
import slope.j8.Rule;
import slowscane.Scanner;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.scanners.TokenReader;
import slowscane.streams.StringBufferStream;
import slowscane.streams.TokenStream;

abstract class Expression {}
class IntExpression extends Expression {}
class IdentifierExpression extends Expression { public String identifier; }
class BracketExpression extends Expression { public Expression childExpression; }
class NFOLDBracketExpression extends Expression { public ArrayList<BracketExpression> bracketExpressions = new ArrayList<BracketExpression>();}


public class TestLangParsingWithParseTree extends TestCase {
	
	public static class TestLangTokens extends slowscane.Tokens {
		@Separator public TokenReader<?> WHITESPACE() { return lib.WHITESPACE; }
		@Separator public TokenReader<?> NEWLINE() { return lib.NEWLINE;}
		@Keyword public TokenReader<String> Int() { return lib.KEYWORD_Int; }
		@Token public TokenReader<String> JAVA_IDENTIFIER() { return lib.JAVA_IDENTIFIER; }
		@Token public TokenReader<?> OBRACKET() { return lib.OPEN_BRACKET; }
		@Token public TokenReader<?> CBRACKET() { return lib.CLOSED_BRACKET; }
		
		
		
		public class TestLangParser extends Parser<TestLangTokens> {
			
			Rule KEYWORD_Int(SetCmd<Expression> resultSetter) {  return () ->
				  // defines that after a successful parsing, a new IntExpression is delivered to the resultSetter.				
				  TOKEN(Int(), setResult(resultSetter, new IntExpression()));
			}	
			
			Rule IDENTIFIER(SetCmd<Expression> resultSetter) { return () -> 
		        TOKEN(JAVA_IDENTIFIER(), 
		        		(value) -> { 
		        			IdentifierExpression expr = new IdentifierExpression();
		        			expr.identifier = value; 
		        			resultSetter.set(expr);});
		    }
			
			Rule N_TIMES_BRACKET_EXPRESSION(SetCmd<Expression> resultSetter) { 

				NFOLDBracketExpression expr = new NFOLDBracketExpression();

				return () ->
				NFOLD(
					BRACKET_EXPRESSION(addResultSetter(expr.bracketExpressions)), 
					setResult(resultSetter, expr));
			}
			
			Rule BRACKET_EXPRESSION(SetCmd<BracketExpression> result) { 
				BracketExpression expr = new BracketExpression();
								
				return () -> 
				AND(
					TOKEN(OBRACKET()), 
					EXPRESSION((value)-> {expr.childExpression = value;}), 
					TOKEN(CBRACKET()), 
					set(()-> result.set(expr)));
			}
			
			Rule EXPRESSION(SetCmd<Expression> result) { return () -> 
					OR(KEYWORD_Int(result), IDENTIFIER(result), N_TIMES_BRACKET_EXPRESSION(result));
			}	
		}
		
	}
	
    public TokenStream<?> scan(String inputString) {
        Tokens tokens = new TestLangTokens();
        StringBufferStream stringBufferStream = new StringBufferStream(inputString);
        Scanner<?, StringBufferStream> scanner = 
            new Scanner<Tokens, StringBufferStream>(tokens, stringBufferStream);
        return scanner.scan();		
    }			
	
	
	public class ResultObject {
		public Expression expression;
	}	
	
	public Expression check(String s) {
		final ResultObject res = new ResultObject();
		SetCmd<Expression> expr = new SetCmd<Expression>() {public void set(Expression value) {
				res.expression = value;	
		}};
		TestLangTokens tokens = new TestLangTokens();
		Parsing p = new Parsing(tokens, s); 
		p.parseWithParseTreeConstruction(tokens.new TestLangParser().EXPRESSION(expr));
		return res.expression;
	}	
	
	public void testParse() {
		  Expression ex = check("Int");
		  assertTrue(ex instanceof IntExpression);
		  
		  ex = check("(Int)");
		  assertTrue( ex instanceof NFOLDBracketExpression);
		  
		  NFOLDBracketExpression nbex = (NFOLDBracketExpression) ex;
		  
		  assertEquals(1,  nbex.bracketExpressions.size());
		  assertTrue(nbex.bracketExpressions.get(0) instanceof BracketExpression);

		  BracketExpression bex = (BracketExpression) nbex.bracketExpressions.get(0);
		  assertTrue(bex.childExpression instanceof IntExpression);
	}		

}