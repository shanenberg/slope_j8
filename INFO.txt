Info:
=====
The projects SLOWSCANE, SHAPUC, and REFLECTIVE_VISITOR are now part of the sources. 

I use the following code to add the projects:

cd /hda1/Dropbox/Development/EclipsePremiumWorkspace/SLOPEJ8/IMPORTED
sudo mount --bind ../../SLOPE/slope ./SLOPEJ5
sudo mount --bind ../../SLOPE/slope/testing ./SLOPEJ5testing
sudo mount --bind ../../SLOPE/sloc ./SLOC
sudo mount --bind ../../SLOWSCANE/SLOWSCANE/src ./SLOWSCANE
sudo mount --bind ../../SHAPUC/src ./SHAPUC
#sudo mount --bind ../..//ReflectiveVisitor/src ./REFLECTIVE_VISITOR

If you have no idea what the text above says, there is probably no need for you to understand it.